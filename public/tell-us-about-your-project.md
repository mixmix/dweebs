# Tell Us About Your project

5mins of people talking about something they give a shit about

---

## Planetary SSB Client - Matt Lorentz

<iframe title="Planetary Reverse Conf" width="560" height="315" src="https://tube.tchncs.de/videos/embed/4af7eb28-9643-45ab-a3a7-4f6720a98c1a" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Imagining Heavens and Hells for Decentralized Archiving on Mobile

...In which we each tell each other something about archiving mobile media to decentralized storage

<iframe title="Imagining Heavens and Hells for Decentralized Archiving on Mobile" width="560" height="315" src="https://www.youtube.com/embed/8VEHNioiFAg" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

-[John Hess](mailto:john@jthess.com) of [Open Archive](https://open-archive.org)

## Mix

<iframe width="560" height="315" src="https://www.youtube.com/embed/SNTcR6t97eE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Likes**:
p2p,
growing community,
sci-fi,
magic

**Links**:
[ahau.io](https://ahau.io),
[scuttlebutt.nz](https://www.scuttlebutt.nz),
[planetary.social](https://www.planetary.social),
[smat-app.com](https://www.smat-app.com)


## Ian Tairea - Project Sunrise

<iframe width="560" height="315" src="https://www.youtube.com/embed/JMeO9JdakqY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Mikey (dinosaur) Williams - Village Kit

<iframe width="560" height="315" src="https://www.youtube.com/embed/qAGGvaGzK3E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Village Kit](https://villagekit.com)
- [Grid Kit](https://gridkit.nz)
- [Mikey (dinosaur) Williams](https://dinosaur.is)

Apologies for going over 5 minutes, thanks for listening. 💜

## Scuttlego, an implementation of the Secure Scuttlebutt protocol

<iframe title="Scuttlego, an implementation of the Secure Scuttlebutt protocol" width="560" height="315" src="https://diode.zone/videos/embed/9956747d-310c-4fb7-a7f1-39582bfa0f02" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

**Links**:
[Scuttlego on Github](https://github.com/planetary-social/scuttlego),
[Planetary on Github](https://github.com/planetary-social/),
[planetary.social](https://www.planetary.social),
[boreq's website](https://0x46.net).

## cblgh on trustnet, cabal and tryna survive as an independent

aka alexander cobleigh

<iframe title="cblgh's intro for dweb camp 2022" width="560" height="315" src="https://www.youtube.com/embed/VitZKSTClWc" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

* https://cblgh.org/about
* https://github.com/cblgh/trustnet
* https://lieu.cblgh.org
* https://github.com/cblgh/plain
* https://github.com/cblgh/monotome

